var dict = {

    "Clarks Beach":"Tab 2",
    "Mellons Bay" :"Tab 3",
    "St Marys Bay Separation":"Tab 4",
    "Takapuna Beach":"Tab 5",
    "Jutland Road WWPS":"Tab 6",
    "Okahu Bay":"Tab 7",
    "Oamaru":"Tab 8",
    "Purewa Separation":"Tab 9",
    "Te Atatu":"Tab 10",
    "Waterview Separation":"Tab 11",
    "Papatoetoe (McLean Ave)":"Tab 12",
    "Capehill WWPS":"Tab 13",
    "Waiheke Island":"Tab 14",
    "Weymouth":"Tab 15",
    "Papatoetoe (Wylie Road)":"Tab 16",
    "New Lynn (Kohekohe St)":"Tab 17",
    "Castor Bay":"Tab 18",
    "Red Beach":"Tab 19",
    "Laingholm Beach":"Tab 20",
    "Blockhouse Bay":"Tab 21",
    "Herne Bay Separation":"Tab 22",
    "Tamaki North (Wai o Taiki Bay)":"Tab 23"

};

var actual_val = $feature.Name;

if (HasKey(dict, actual_val)) {
    return dict[actual_val];
} else {
    return "n/a";
}
